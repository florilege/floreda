<?php

namespace Florilege\RedactionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RedactionController extends Controller
{
    public function indexAction()
    {
        return $this->render('FlorilegeRedactionBundle:Redaction:index.html.twig');
    }

    public function connectionAction()
    {
        return $this->render('FlorilegeRedactionBundle:Redaction:connection.html.twig');
    }

    public function dashboardAction()
    {
        return $this->render('FlorilegeRedactionBundle:Redaction:dashboard.html.twig');
    }

    public function accountAction()
    {
        return $this->render('FlorilegeRedactionBundle:Redaction:account.html.twig');
    }

}
